package com.example.atmosphere.issue;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Used to configure the Jersey application/resources. 
 */
@ApplicationPath("rest")
public class JerseyApplication extends ResourceConfig {
    public JerseyApplication() {
        packages("com.example.atmosphere");
    }
}