package com.example.atmosphere.issue;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Generic class representing a notification containing
 * an action and an object that was acted upon.
 * 
 *  @param <T> the data type related to the notification.
 */
public class Notification<T> {

	public enum ActionType {
		CREATED, UPDATED, DELETED
	};

	private final ActionType action;

	private final T object;

	@JsonCreator
	public Notification(@JsonProperty("action") ActionType action,
			@JsonProperty("object") T object) {
		this.action = action;
		this.object = object;
	}

	public ActionType getAction() {
		return action;
	}

	public T getObject() {
		return object;
	}

	@Override
	public String toString() {
		return "Notification [action=" + action + ", object=" + object + "]";
	}
}
