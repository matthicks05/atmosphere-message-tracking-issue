package com.example.atmosphere.issue;

import java.io.IOException;

import org.atmosphere.config.managed.Encoder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NotificationEncoder<T> implements Encoder<Notification<T>, String> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String encode(Notification<T> m) {
        try {
            return mapper.writeValueAsString(m);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
