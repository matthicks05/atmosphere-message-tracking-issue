# Atmosphere TrackMessageSizeInterceptor Issue

This project is an example of the issue being discussed [here](https://groups.google.com/d/msg/atmosphere-framework/ZoOVY8VItOA/YbTmrWM2NrsJ).

# Running the Example

Run the server.

```
mvn jetty:run -Djetty.port=9090
```

Run the tests.

```
mvn test
```

# Results

Note that you will see the following as output of the failing test:

```
09:15:23.497 [New I/O worker #10] WARN  c.e.a.MultipleSubscriptionTest - Invalid message 39|36|{"action":"UPDATED","object":"Bar!"}
09:15:23.499 [New I/O worker #1] INFO  c.e.a.MultipleSubscriptionTest - Received notification Notification [action=UPDATED, object=Foo!]
```

The first message is unable to be decoded because it exhibits the issue where it has been processed by two TrackMessageSizeInterceptor instances.

One workaround is to comment out one of the TrackMessageSizeInterceptor interceptors for one of the @ManagedService's.