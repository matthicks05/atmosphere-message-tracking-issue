package com.example.atmosphere.issue;

import java.io.IOException;

import javax.ws.rs.POST;

import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Heartbeat;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.IdleResourceInterceptor;
import org.atmosphere.interceptor.SuspendTrackerInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.atmosphere.issue.Notification.ActionType;

@ManagedService(path = "/subs/foo", interceptors = {
		AtmosphereResourceLifecycleInterceptor.class,
		TrackMessageSizeInterceptor.class, 
		IdleResourceInterceptor.class,
		SuspendTrackerInterceptor.class })
public class FooManagedService {

	private final Logger logger = LoggerFactory
			.getLogger(FooManagedService.class);

	private Broadcaster broadcaster;

	@Heartbeat
	public void onHeartbeat(final AtmosphereResourceEvent event) {
		logger.info("Heartbeat send by {}", event.getResource());
	}

	/**
	 * Invoked when the connection as been fully established and suspended, e.g
	 * ready for receiving messages.
	 * 
	 * @param r
	 */
	@Ready
	public void onReady(final AtmosphereResource r) {
		logger.info("Client {} connected.", r.uuid());
		broadcaster = r.getBroadcaster();
	}

	/**
	 * Invoked when the client disconnect or when an unexpected closing of the
	 * underlying connection happens.
	 * 
	 * @param event
	 */
	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {
		if (event.isCancelled()) {
			logger.info("Client {} unexpectedly disconnected", event
					.getResource().uuid());
		} else if (event.isClosedByClient()) {
			logger.info("Client {} closed the connection", event.getResource()
					.uuid());
		}
	}

	@Message(encoders = { NotificationEncoder.class } )
	public Notification<String> onMessage(Notification<String> e)
			throws IOException {
		logger.info("Sending message with Notification: {}", e.getObject());
		return e;
	}
	
	@POST
	public void publish()
	{
		Notification<String> notif = new Notification<String>(ActionType.UPDATED, "Foo!");		
		broadcaster.broadcast(notif);
	}
}