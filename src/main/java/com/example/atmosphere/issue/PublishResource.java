package com.example.atmosphere.issue;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.atmosphere.cpr.BroadcasterFactory;

import com.example.atmosphere.issue.Notification.ActionType;

@Path("/publish")
public class PublishResource {

	@Path("/foo")
	@GET
	public void publishFoo()
	{
		BroadcasterFactory.getDefault().lookup("/subs/foo", true).broadcast(new Notification<String>(ActionType.UPDATED, "Foo!"));
	}
	
	@Path("/bar")
	@GET
	public void publishBar()
	{
		BroadcasterFactory.getDefault().lookup("/subs/bar", true).broadcast(new Notification<String>(ActionType.UPDATED, "Bar!"));		
	}
}
