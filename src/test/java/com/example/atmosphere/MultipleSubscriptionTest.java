package com.example.atmosphere;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.ClientBuilder;

import org.atmosphere.wasync.ClientFactory;
import org.atmosphere.wasync.Decoder;
import org.atmosphere.wasync.Function;
import org.atmosphere.wasync.Request;
import org.atmosphere.wasync.RequestBuilder;
import org.atmosphere.wasync.Socket;
import org.atmosphere.wasync.impl.AtmosphereClient;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.atmosphere.issue.Notification;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration test that exercises the {@link BasicTrajectorySubscriptionClient}
 * to:
 * <ol>
 * <li>Test subscription to a single {@link Trajectory}</li>
 * <li>Test subscription to all {@link Trajectory} objects for a given
 * {@link WellboreRef}</li>
 * </ol>
 */
public class MultipleSubscriptionTest {

	private static final Logger logger = LoggerFactory
			.getLogger(MultipleSubscriptionTest.class);

	private static final String BASE_URL = "http://localhost:9090";

	private static final int TIMEOUT = 5000;

	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void testMultipleSubscription() throws Exception {
		
		TestFunction fooFunction = new TestFunction();		
		subscribe("/subs/foo", fooFunction);

		TestFunction barFunction = new TestFunction();
		subscribe("/subs/bar", barFunction);

		// Publish Foo
		ClientBuilder.newClient().target(BASE_URL).path("/rest/publish/foo").request().get();

		// Publish Bar
		ClientBuilder.newClient().target(BASE_URL).path("/rest/publish/bar").request().get();

		// Give it a sec...
		Thread.sleep(1000);

		assertTrue(fooFunction.notified);
		assertTrue(barFunction.notified);		
	}

	private <T> void subscribe(String topic, Function<T> function)
			throws Exception {
		AtmosphereClient client = ClientFactory.getDefault().newClient(
				AtmosphereClient.class);

		RequestBuilder request = client
				.newRequestBuilder()
				.method(Request.METHOD.GET)
				.uri(BASE_URL + topic)
				.decoder(new Decoder<String, Notification<String>>() {
					@Override
					public Notification<String> decode(
							org.atmosphere.wasync.Event event, String data) {
						data = data.trim();

						// Padding
						if (data.length() == 0) {
							return null;
						}

						if (event.equals(org.atmosphere.wasync.Event.MESSAGE)) {
							try {
								JavaType javaType = mapper.getTypeFactory()
										.constructParametricType(
												Notification.class,
												String.class);
								return mapper.readValue(data, javaType);
							} catch (IOException e) {
								logger.warn("Invalid message " + data);
								return null;
							}
						} else {
							return null;
						}
					}

				}).trackMessageLength(true)
				.transport(Request.TRANSPORT.WEBSOCKET);

		Socket socket = client.create();
		socket.on(function).open(request.build(), TIMEOUT,
				TimeUnit.MILLISECONDS);
	}
	
	private class TestFunction implements Function<Notification<String>> {

		public boolean notified;
		
		@Override
		public void on(Notification<String> t) {			
			notified = true;
			logger.info("Received notification {}", t);
		}
		
	}

}
